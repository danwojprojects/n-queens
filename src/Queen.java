public class Queen {
    private int row;
    private int column;

    Queen(int row, int column) {
        this.row = row;
        this.column = column;
    }

    boolean inConflict(Queen q){
        //  Check rows and columns
        if(row == q.getRow() || column == q.getCol())
            return true;
            //  Check diagonals
        else if(Math.abs(column-q.getCol()) == Math.abs(row-q.getRow()))
            return true;

        return false;
    }

    int getRow() {
        return row;
    }

    int getCol() {
        return column;
    }


    void down() {
        row++;
    }

    public String toString() {
        return row + "," + column;
    }
}